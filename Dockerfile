FROM openjdk:8-jre

ADD ./source/build/libs/tchallenge-service.jar /app/app.jar

CMD ["java", "-jar", "/app/app.jar"]